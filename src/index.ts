import { render } from "react-dom";
import { createElement } from "react";
import App from "./App";

const root = document.createElement("div");
document.body.appendChild(root);
render(createElement(App, {}), root);
