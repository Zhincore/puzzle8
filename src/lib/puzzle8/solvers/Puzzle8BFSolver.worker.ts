import { expose } from "threads/worker";
import Solver from "./Puzzle8BFSolver";

expose(Solver);
