import { Observable } from "observable-fns";
import { Puzzle8Field, generatePossibleMoves, fieldsEqual } from "../Puzzle8";

export type SolutionBranch<NSize extends number> = {
  root?: SolutionBranch<NSize>;
  parent?: SolutionBranch<NSize>;
  field: Puzzle8Field<NSize>;
  children: SolutionBranch<NSize>[];
};

export type Puzzle8BFSolverFailure = "maxNodesReached";
export type Puzzle8BFSolverProgress<NSize extends number> = {
  nSteps: number;
  nNodes: number;
  solution?: SolutionBranch<NSize>;
  failed?: Puzzle8BFSolverFailure;
  branches?: SolutionBranch<NSize>[];
};

export type Puzzle8BFSolverOptions = {
  maxNodes: number;
  reportPer: number;
  targetBranches?: number;
};

export function runSolver<NSize extends number>(
  init: SolutionBranch<NSize>[],
  goal: Puzzle8Field<NSize>,
  options: Puzzle8BFSolverOptions,
) {
  return new Observable<Puzzle8BFSolverProgress<NSize>>((observer) => {
    const { maxNodes, reportPer, targetBranches } = options;
    let nNodesAcc = 0;
    let nNodes = 0;
    let nSteps = 0;
    let lastSolutions: SolutionBranch<NSize>[] = init;
    let solution: SolutionBranch<NSize> | undefined;
    let shouldRun = true;

    for (const { field } of lastSolutions) {
      if (fieldsEqual(field, goal)) {
        const solution: SolutionBranch<NSize> = {
          children: [],
          field,
        };
        observer.next({ nNodes, nSteps, solution });
        return observer.complete();
      }
    }

    const step = () => {
      const solutions: SolutionBranch<NSize>[] = [];
      nSteps++;

      for (let i = 0; i < lastSolutions.length; i++) {
        const lastSolution = lastSolutions[i];

        const moves = generatePossibleMoves(lastSolution.field);

        for (const field of moves) {
          nNodes++;
          nNodesAcc++;

          const branch: SolutionBranch<NSize> = {
            children: [],
            field,
            parent: lastSolution,
            root: lastSolution.root ?? lastSolution,
          };

          solutions.push(branch);
          lastSolution.children.push(branch);

          if (fieldsEqual(field, goal)) {
            cleanupSolutionTree(branch);
            solution = branch;
            break;
          }
        }

        if (solution) {
          observer.next({ nNodes, nSteps, solution });
          shouldRun = false;
          break;
        } else if (targetBranches && solutions.length >= targetBranches) {
          shouldRun = false;
        } else if (nNodes >= maxNodes) {
          observer.next({ nNodes, nSteps, failed: "maxNodesReached" });
          shouldRun = false;
          break;
        } else {
          if (nNodesAcc >= reportPer) {
            nNodesAcc %= reportPer;
            observer.next({ nNodes, nSteps });
          }
        }
      }

      lastSolutions = solutions;

      if (!shouldRun) {
        if (targetBranches && !solution) {
          observer.next({ nNodes, nSteps, branches: lastSolutions });
        } else {
          observer.next({ nNodes, nSteps });
        }

        return observer.complete();
      }

      observer.next({ nNodes, nSteps });
      setTimeout(step);
    };

    step();
  });
}

export function solve<NSize extends number>(
  init: Puzzle8Field<NSize>,
  goal: Puzzle8Field<NSize>,
  options: Puzzle8BFSolverOptions,
) {
  return runSolver([{ field: init, children: [] }], goal, options);
}

const Puzzle8BFSolver = { runSolver, solve };
export type Puzzle8BFSolver = typeof Puzzle8BFSolver & {
  [k: string]: any;
};
export default Puzzle8BFSolver;

export function cleanupSolutionTree<NSize extends number>(
  solution: SolutionBranch<NSize>,
  child?: SolutionBranch<NSize>,
) {
  solution.children = child ? [child] : [];
  if (solution.parent) cleanupSolutionTree(solution.parent, solution);
}
