import { Field2D } from "./util";

export type Puzzle8Field<NSize extends number> = Field2D<number, NSize>;
export type Point = [number, number];

export function generatePossibleMoves<NSize extends number>(
  field: Puzzle8Field<NSize>,
  history?: Puzzle8Field<NSize>[],
): Puzzle8Field<NSize>[] {
  const hole = findHole(field);
  const fields: Puzzle8Field<NSize>[] = [];

  for (let x = -1; x <= 1; x++) {
    const a = Number(x === 0);
    for (let y = -a; y <= a; y++) {
      const cell = (field[hole[1] + y] ?? [])[hole[0] + x];
      if (!cell) continue;

      const newField = cloneField(field);
      newField[hole[1] + y][hole[0] + x] = 0;
      newField[hole[1]][hole[0]] = cell;

      if (history && history.some((_field) => fieldsEqual(_field, newField))) continue;

      fields.push(newField);
    }
  }

  return fields;
}

export function findHole<NSize extends number>(field: Puzzle8Field<NSize>): Point {
  for (let y = field.length - 1; y >= 0; y--) {
    for (let x = field[y].length - 1; x >= 0; x--) {
      if (!field[y][x]) return [x, y];
    }
  }
  throw new Error("Given field doesn't have a hole");
}

export function createField<NSize extends number>(size: NSize): Puzzle8Field<NSize> {
  return new Array(size)
    .fill(0)
    .map((_, y) => new Array(size).fill(0).map((_, x) => y * size + x)) as Puzzle8Field<NSize>;
}

export function cloneField<NFieldSize extends number>(field: Puzzle8Field<NFieldSize>) {
  return field.map((row) => [...row]) as Puzzle8Field<NFieldSize>;
}

export function fieldsEqual<NFieldSize extends number>(
  field1: Puzzle8Field<NFieldSize>,
  field2: Puzzle8Field<NFieldSize>,
) {
  return field1.toString() === field2.toString();
}
