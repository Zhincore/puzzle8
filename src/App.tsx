import { useState, useEffect } from "react";
import { ChakraProvider, Stack, StackItem, StackDivider, VStack, Box, Flex, Heading } from "@chakra-ui/react";
import CustomNumberInput from "~/components/CustomNumberInput";
import Puzzle8Input from "~/components/Puzzle8Input";
import Solver from "~/components/Solver";
import theme from "~/lib/theme";
import { Puzzle8Field, createField } from "~/lib/puzzle8";

export default function App() {
  const [size, setSize] = useState(3);
  const [initField, setInitField] = useState<Puzzle8Field<typeof size>>(() => createField(size));
  const [goalField, setGoalField] = useState<Puzzle8Field<typeof size>>(() => createField(size));

  useEffect(() => {
    setInitField(createField(size));
    setGoalField(createField(size));
  }, [size]);

  return (
    <ChakraProvider theme={theme}>
      <VStack p={3}>
        <Flex alignItems="center" lineHeight={1}>
          <Heading as="h1" size="2xl">
            Puzzle8
          </Heading>
          <Box as="span" color="gray.400" ml={3}>
            &ndash; Adam Žingor
          </Box>
        </Flex>
        <Stack
          direction={["column", "row"]}
          p={3}
          spacing={8}
          w="100%"
          justifyContent="center"
          divider={<StackDivider borderColor="gray.600" />}
        >
          <VStack p={3} flex={1} alignItems={["center", "flex-end"]}>
            <Heading size="xl" w="100%" textAlign="right">
              Vstup
            </Heading>
            <Stack>
              <Box m={3}>
                <CustomNumberInput label="Velikost pole" value={size} min={2} step={1} setValue={setSize} />
              </Box>
            </Stack>
            <Stack direction={["column", "row", "row"]}>
              <StackItem>
                <Puzzle8Input field={initField} onReorder={setInitField} title="Počáteční stav" />
              </StackItem>
              <StackItem>
                <Puzzle8Input field={goalField} onReorder={setGoalField} title="Cílový stav" />
              </StackItem>
            </Stack>
          </VStack>

          <StackItem p={3} flex={1}>
            <Heading size="xl">Výstup</Heading>
            <Solver initField={initField} goalField={goalField} />
          </StackItem>
        </Stack>
      </VStack>
    </ChakraProvider>
  );
}
