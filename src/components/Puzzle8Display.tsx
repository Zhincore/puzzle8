import { motion, PanInfo } from "framer-motion";
import { Grid, GridItem, GridItemProps, BoxProps } from "@chakra-ui/react";
import { Puzzle8Field, cloneField, Point } from "~/lib/puzzle8";

const WIDTH = 320;
const HEIGHT = 320;
const FONT_SIZE = 128;
const GRID_GAP = 0.05;

export type Puzzle8DisplayProps<NSize extends number> = BoxProps & {
  width?: number;
  height?: number;
  fontSize?: number;
  readonly?: boolean;
  field: Puzzle8Field<NSize>;
  onReorder?: (field: Puzzle8Field<NSize>) => void;
};

const MotionGridItem = motion<GridItemProps>(GridItem);

export default function Puzzle8Display<NSize extends number>({
  field,
  readonly,
  width,
  height,
  fontSize,
  onReorder,
  ...props
}: Puzzle8DisplayProps<NSize>) {
  width = width ?? WIDTH;
  height = height ?? HEIGHT;
  fontSize = fontSize ?? FONT_SIZE;
  const cellWidth = width / field.length;
  const cellHeight = height / field.length;
  const gridGap = Math.min(cellWidth, cellHeight) * GRID_GAP;

  const getOnDragEndHandler = (x: number, y: number, value: any) => (_: any, info?: PanInfo) => {
    if (!info || !onReorder) return;
    const vec: Point = [info.offset.x / cellWidth, info.offset.y / cellHeight];
    const target = [x + vec[0], y + vec[1]].map((a) => Math.round(Math.min(field.length - 1, Math.max(0, a))));
    const newField = cloneField(field);
    newField[target[1]][target[0]] = value;
    newField[y][x] = field[target[1]][target[0]];
    onReorder(newField);
  };

  return (
    <Grid
      gridTemplate={`repeat(${field.length}, 1fr) / repeat(${field.length}, 1fr)`}
      gridGap={gridGap + "px"}
      maxW="100%"
      w={width + "px"}
      maxH="100%"
      h={height + "px"}
      fontSize={((fontSize / WIDTH) * width) / field.length}
      {...props}
    >
      {field.flatMap((row, y) =>
        row.map((cell, x) => {
          if (!cell) return <GridItem key={cell} />;
          /* const vec = [hole[0] - x, hole[1] - y];
          const dragConstraints =
            Math.abs(vec[0]) === 1 && vec[1] === 0
              ? { left: Math.max(0, -vec[0]), right: Math.max(0, vec[0]) }
              : vec[0] === 0 && Math.abs(vec[1]) === 1
              ? { top: Math.max(0, -vec[1]), bottom: Math.max(0, vec[1]) }
              : {}; */
          // dragElastic={{ top: 0, left: 0, bottom: 0, right: 0, ...dragConstraints }}

          return (
            <MotionGridItem
              key={cell}
              layout
              bgColor="gray.600"
              lineHeight={1}
              pt={"10%"}
              display="flex"
              justifyContent="center"
              alignItems="center"
              drag={!readonly}
              style={{ width: cellWidth - gridGap, height: cellHeight - gridGap }}
              dragConstraints={{ top: 0, left: 0, bottom: 0, right: 0 }}
              dragElastic={0.9}
              dragMomentum={false}
              cursor={readonly ? undefined : "grab"}
              _active={{ cursor: readonly ? undefined : "grabbing" }}
              onDragEnd={getOnDragEndHandler(x, y, cell)}
            >
              {cell}
            </MotionGridItem>
          );
        }),
      )}
    </Grid>
  );
}
