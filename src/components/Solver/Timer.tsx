import { useState, useEffect } from "react";
import ms from "pretty-ms";

type TimerProps = {
  isActive: boolean;
  reset?: boolean;
  delay?: number;
};

export default function Timer({ isActive, delay, reset }: TimerProps) {
  const [startTime, setStartTime] = useState(0);
  const [currentTime, setCurrentTime] = useState(0);

  useEffect(() => {
    setStartTime(0);
    setCurrentTime(0);
  }, [reset]);

  useEffect(() => {
    if (!isActive) return;

    setStartTime(Date.now());
    setCurrentTime(Date.now());
    const interval = setInterval(() => setCurrentTime(Date.now()), delay ?? 100);
    return () => {
      clearInterval(interval);
    };
  }, [isActive, delay]);

  return <>{ms(currentTime - startTime, { keepDecimalsOnWholeSeconds: true })}</>;
}
