import { useState, useEffect, useMemo } from "react";
import {
  Box,
  BoxProps,
  Stack,
  VStack,
  Flex,
  ButtonGroup,
  Button,
  IconButton,
  Heading,
  Wrap,
  WrapItem,
} from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlay, faPause, faStepBackward, faStepForward } from "@fortawesome/free-solid-svg-icons";
import { motion, LayoutGroup } from "framer-motion";
import Puzzle8Display from "../Puzzle8Display";
import { SolutionBranch } from "~/lib/puzzle8/solvers/Puzzle8BFSolver";

type SolutionPlayerProps<NSize extends number> = {
  solution: SolutionBranch<NSize>;
};

const MotionBox = motion<BoxProps>(Box);

export default function SolutionPlayer<NSize extends number>({ solution }: SolutionPlayerProps<NSize>) {
  const history = useMemo(() => getMoves(solution), [solution]);
  const [step, setStep] = useState(history.length - 1);
  const [playing, setPlaying] = useState(false);
  const ended = step === history.length - 1;

  useEffect(() => {
    setStep(history.length - 1);
    setPlaying(false);
  }, [history]);

  const _setStep = (step: number) => {
    if (playing) setPlaying(false);
    setStep((history.length + step) % history.length);
  };

  const playNext = (force?: true) => {
    if (!force && !playing) return;
    if (!force && ended) setPlaying(false);
    setPlaying((_playing) => {
      if (!_playing || (!force && ended)) return false;
      setStep((n) => n + 1);
      return true;
    });
  };

  const startPlaying = () => {
    if (playing) return;
    if (ended) setStep(-1);
    setPlaying(true);
    playNext(true);
  };

  return (
    <Stack direction={["column", "row"]} spacing={8}>
      <VStack>
        <Puzzle8Display readonly field={history[step]} />

        <ButtonGroup>
          <IconButton
            icon={<FontAwesomeIcon icon={faStepBackward} />}
            aria-label="Krok vzad"
            isDisabled={step === 0}
            onClick={() => _setStep(step - 1)}
          />
          <Button
            leftIcon={<FontAwesomeIcon icon={playing ? faPause : faPlay} />}
            isDisabled={history.length < 2}
            onClick={playing ? () => setPlaying(false) : startPlaying}
          >
            {playing ? "Zastavit" : "Přehrát"}
          </Button>
          <IconButton
            icon={<FontAwesomeIcon icon={faStepForward} />}
            aria-label="Krok vpřed"
            isDisabled={ended}
            onClick={() => _setStep(step + 1)}
          />
        </ButtonGroup>
      </VStack>

      <Box>
        <Flex alignItems="center" lineHeight={1}>
          <Heading as="h4" size="md">
            Kroky
          </Heading>
          <Box as="span" ml={2} color="gray.400">
            &ndash; krok {step + 1} / {history.length}
          </Box>
        </Flex>
        <Wrap as={LayoutGroup} spacing={2} my={4}>
          {history.map((field, i) => (
            <WrapItem key={i} cursor="pointer" position="relative" onClick={() => _setStep(i)}>
              <Puzzle8Display field={field} readonly width={64} height={64} fontSize={256} />
              {i === step && (
                <MotionBox
                  position="absolute"
                  top={"-4px"}
                  left={"-4px"}
                  right={"-4px"}
                  bottom={"-4px"}
                  border="2px"
                  borderColor="blue.200"
                  borderRadius="2px"
                  layoutId="solutionStepFrame"
                  onLayoutAnimationComplete={playNext}
                />
              )}
            </WrapItem>
          ))}
        </Wrap>
      </Box>
    </Stack>
  );
}

function getMoves<NSize extends number>(solution: SolutionBranch<NSize>) {
  const moves = [solution.field];
  if (solution.parent) moves.unshift(...getMoves(solution.parent));
  return moves;
}
