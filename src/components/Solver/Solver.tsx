import { useState, useEffect } from "react";
import { ModuleThread, Pool, spawn } from "threads";
import {
  VStack,
  Stack,
  Box,
  Button,
  StatGroup,
  Stat,
  StatLabel,
  StatNumber,
  Alert,
  AlertTitle,
  AlertDescription,
  Heading,
  Switch,
  FormControl,
  FormLabel,
} from "@chakra-ui/react";
import CustomNumberInput from "../CustomNumberInput";
import Timer from "./Timer";
import SolutionPlayer from "./SolutionPlayer";
import {
  Puzzle8BFSolver,
  SolutionBranch,
  Puzzle8BFSolverFailure,
  Puzzle8BFSolverProgress,
} from "~/lib/puzzle8/solvers/Puzzle8BFSolver";
import { Puzzle8Field } from "~/lib/puzzle8";

const MAX_NODES = 4_000_000;
const REPORT_PER = 50_000;
const NTHREADS = Math.max((navigator.hardwareConcurrency ?? 2) - 1, 1);

type SolverProps<NSize extends number> = {
  initField: Puzzle8Field<NSize>;
  goalField: Puzzle8Field<NSize>;
};

type Puzzle8BFSolverThread = ModuleThread<Puzzle8BFSolver>;
const spawnWorker = () =>
  spawn<Puzzle8BFSolverThread>(
    new Worker(new URL("~/lib/puzzle8/solvers/Puzzle8BFSolver.worker", import.meta.url) as any as string),
  );

const failTranslations: Record<Puzzle8BFSolverFailure, string> = {
  maxNodesReached: "Dosaženo maximálního počtu uzlů.",
};

export default function Solver<NSize extends number>({ initField, goalField }: SolverProps<NSize>) {
  const [maxNodes, setMaxNodes] = useState(MAX_NODES);
  const [multithread, setMultithread] = useState(true);
  const [solution, setSolution] = useState<SolutionBranch<NSize>>();
  const [solver, setSolver] = useState<Pool<Puzzle8BFSolverThread>>();
  const [failed, setFailed] = useState<Puzzle8BFSolverFailure>();
  const [nsteps, setNSteps] = useState(0);
  const [nnodes, setNNodes] = useState(0);
  const nthreads = multithread ? NTHREADS : 1;

  const reset = async () => {
    if (solver) {
      solver.terminate(true);
      setSolver(undefined);
    }
    setSolution(undefined);
    setFailed(undefined);
    setNSteps(0);
    setNNodes(0);
  };

  const start = async () => {
    await reset();

    const maxNodesPerThread = Math.round(maxNodes / NTHREADS);
    const _solver = Pool<Puzzle8BFSolverThread>(spawnWorker, nthreads);
    setSolver(() => _solver);

    let presolveSteps = 0;
    let fail: Puzzle8BFSolverFailure | undefined;
    const processTask = async (obs: ReturnType<Puzzle8BFSolverThread["runSolver"]>) => {
      let lastNNodes = 0;
      let solved = false;
      let lastProgress: Puzzle8BFSolverProgress<NSize>;

      obs.subscribe((progress) => {
        if (progress.solution) {
          solved = true;
          setSolution((v) => (v ? v : (progress.solution as any)));
        } else if (progress.failed) fail = progress.failed;

        setNNodes((n) => n + (progress.nNodes - lastNNodes));
        setNSteps((n) => Math.max(n, progress.nSteps + presolveSteps));
        lastNNodes = progress.nNodes;
        lastProgress = progress as any;
      });
      return obs.then(() => {
        if (solved) _solver.terminate(true);
        return lastProgress;
      });
    };

    let presolve: Puzzle8BFSolverProgress<NSize> | undefined = await _solver.queue((worker) =>
      processTask(
        worker.solve(initField, goalField, {
          maxNodes: multithread ? maxNodesPerThread : maxNodes,
          reportPer: REPORT_PER,
          targetBranches: multithread ? NTHREADS : undefined,
        }),
      ),
    );
    presolveSteps = presolve!.nSteps;

    if (presolve!.solution || fail) {
      _solver.terminate(true);
      if (fail) {
        setFailed(fail);
      }
    } else if (multithread && presolve!.branches) {
      const nBranches = presolve!.branches.length;
      const promises: Promise<any>[] = [];
      for (let i = NTHREADS - 1; i >= 0; i--) {
        promises.push(
          _solver
            .queue(
              (
                (_presolve) => (worker) =>
                  processTask(
                    worker.runSolver(
                      _presolve!.branches!.slice(
                        Math.floor((i / NTHREADS) * nBranches),
                        Math.floor(((i + 1) / NTHREADS) * nBranches),
                      ) as any,
                      goalField,
                      {
                        maxNodes: maxNodesPerThread,
                        reportPer: REPORT_PER,
                      },
                    ),
                  )
              )(presolve),
            )
            .then(),
        );
      }
      Promise.all(promises).then(() => {
        if (fail) {
          setFailed(fail);
        }
      });
    }
    // remove reference
    presolve = undefined;
  };

  useEffect(() => {
    reset();
  }, [initField, goalField]);

  const isRunning = !!solver && !solution && !failed;

  return (
    <VStack my={3} spacing={6} alignItems={["center", "flex-start"]}>
      <Stack direction="row" spacing={3} alignItems="flex-end" w="100%">
        <Button isLoading={isRunning} loadingText="Řešení..." isDisabled={!!failed} onClick={start}>
          Vyřešit
        </Button>
        <Button onClick={reset} isDisabled={!solution && !solver}>
          {isRunning ? "Zrušit" : "Reset"}
        </Button>
        <CustomNumberInput w="auto" label="Max. stavů" value={maxNodes} setValue={setMaxNodes} isDisabled={isRunning} />
        <FormControl w="auto" isDisabled={isRunning}>
          <FormLabel fontWeight="bold" htmlFor="multithread">
            Multi-threading
          </FormLabel>
          <Switch
            id="multithread"
            isDisabled={isRunning}
            isChecked={multithread}
            onChange={() => setMultithread(!multithread)}
          />
        </FormControl>
      </Stack>

      <StatGroup m={3} minW="320" w="100%">
        <Stat>
          <StatLabel mb={2}>Počet kroků</StatLabel>
          <StatNumber lineHeight={1}>{nsteps.toLocaleString()}</StatNumber>
        </Stat>
        <Stat>
          <StatLabel mb={2}>Počet stavů (uzlů)</StatLabel>
          <StatNumber lineHeight={1}>
            {nnodes.toLocaleString()}
            <Box as="span" color="gray.600" fontSize="md">
              {" / " + maxNodes.toLocaleString()}
            </Box>
          </StatNumber>
        </Stat>
        <Stat>
          <StatLabel mb={2}>Čas</StatLabel>
          <StatNumber lineHeight={1}>
            <Timer isActive={isRunning} reset={!solver} />
          </StatNumber>
        </Stat>
        <Stat>
          <StatLabel mb={2}>Vláken</StatLabel>
          <StatNumber lineHeight={1}>{nthreads}</StatNumber>
        </Stat>
      </StatGroup>

      {failed && (
        <Alert colorScheme="red">
          <AlertTitle>Hledání řešení selhalo.</AlertTitle>
          <AlertDescription>{failTranslations[failed]}</AlertDescription>
        </Alert>
      )}

      {solution && (
        <Box>
          <Heading as="h3" size="lg">
            Řešení
          </Heading>
          <SolutionPlayer solution={solution} />
        </Box>
      )}
    </VStack>
  );
}
