import { useRef } from "react";
import { Box, Heading, Button, ButtonGroup } from "@chakra-ui/react";
import Puzzle8Display, { Puzzle8DisplayProps } from "./Puzzle8Display";
import { createField, Puzzle8Field, generatePossibleMoves } from "~/lib/puzzle8";

type Puzzle8InputProps<NSize extends number> = Omit<Puzzle8DisplayProps<NSize>, "field" | "setField"> & {
  title: string;
  field: Puzzle8Field<NSize>;
  onReorder: (field: Puzzle8Field<NSize>) => void;
};

export default function Puzzle8Input<NSize extends number>({
  title,
  field,
  onReorder,
  ...props
}: Puzzle8InputProps<NSize>) {
  const historyRef = useRef<Puzzle8Field<NSize>[]>([]);

  const randomize = () => {
    historyRef.current.push(field);
    while (historyRef.current.length > field.length + 1) historyRef.current.shift();

    const moves = generatePossibleMoves(field, historyRef.current);
    onReorder(moves[Math.floor(Math.random() * moves.length)]);
  };

  const reset = () => onReorder(createField(field.length) as Puzzle8Field<NSize>);

  return (
    <Box m={3}>
      <Heading as="h3" size="lg">
        {title}
      </Heading>
      <Puzzle8Display {...props} field={field} onReorder={onReorder} my={2} />
      <ButtonGroup>
        <Button onClick={randomize}>Náhodný tah</Button>
        <Button variant="outline" onClick={reset}>
          Reset
        </Button>
      </ButtonGroup>
    </Box>
  );
}
