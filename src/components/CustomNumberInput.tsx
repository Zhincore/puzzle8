import {
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  FormControl,
  FormControlProps,
  FormLabel,
} from "@chakra-ui/react";

type CustomNumberInputProps = FormControlProps & {
  label: string;
  value: number;
  setValue: (size: number) => void;
  min?: number;
  step?: number;
  max?: number;
};

export default function CustomNumberInput({
  label,
  value,
  setValue,
  min,
  max,
  step,
  ...props
}: CustomNumberInputProps) {
  return (
    <FormControl {...props}>
      <FormLabel htmlFor="fieldSizeInput" mb={0} fontWeight="bold">
        {label}
      </FormLabel>
      <NumberInput value={value} min={min} step={step} max={max} onChange={(_, v) => setValue(v)} w={32}>
        <NumberInputField id="fieldSizeInput" />
        <NumberInputStepper>
          <NumberIncrementStepper />
          <NumberDecrementStepper />
        </NumberInputStepper>
      </NumberInput>
    </FormControl>
  );
}
