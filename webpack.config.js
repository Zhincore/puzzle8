/* eslint-disable @typescript-eslint/no-var-requires */
const path = require("path");
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const tsconfigPaths = require("./tsconfig.json");

const development = process.env.NODE_ENV !== "production";

const f = (str) => str.replace("/*", "");

module.exports = {
  mode: development ? "development" : "production",
  entry: {
    main: "./src/index.ts",
  },
  output: {
    path: path.resolve(__dirname, "./dist"),
    publicPath: development ? "/" : "./",
    filename: "[name].js",
    clean: true,
  },
  devtool: "inline-source-map",
  devServer: {
    hot: true,
    historyApiFallback: true,
  },
  resolve: {
    roots: [path.resolve(__dirname, "./src")],
    extensions: [".ts", ".tsx", ".js"],
    alias: {
      ...Object.entries(tsconfigPaths.compilerOptions.paths).reduce(
        (acc, [key, val]) => (acc[f(key)] = path.resolve(__dirname, "./" + f(val[0]))) && acc,
        {},
      ),
    },
  },
  module: {
    rules: [
      {
        test: /\.(j|t)sx?$/,
        exclude: [/node_modules/],
        use: [
          {
            loader: "babel-loader",
            options: {
              sourceType: "module",
              cacheDirectory: true,
              presets: [
                ["@babel/preset-env", { targets: "last 2 versions", useBuiltIns: "usage", corejs: "3.16" }],
                ["@babel/preset-react", { runtime: "automatic", development }],
                ["@babel/preset-typescript"],
              ],
              plugins: [development && "react-refresh/babel"].filter(Boolean),
            },
          },
        ],
      },
      {
        test: /\.s?(c|a)ss$/,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({ title: "Puzzle8" }),
    new MiniCssExtractPlugin(),
    development && new ReactRefreshWebpackPlugin(),
  ].filter(Boolean),
};
